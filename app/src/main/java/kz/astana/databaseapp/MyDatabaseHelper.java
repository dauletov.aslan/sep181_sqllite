package kz.astana.databaseapp;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class MyDatabaseHelper extends SQLiteOpenHelper {

    public MyDatabaseHelper(Context context, String dbName, SQLiteDatabase.CursorFactory cursorFactory, int version) {
        super(context, dbName, cursorFactory, version);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        Log.d("Hello", "Database created");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("CREATE TABLE animals(id INTEGER PRIMARY KEY AUTOINCREMENT, name TEXT);");
        Log.d("Hello", "Database updated");
        Log.d("Hello", "Old version: " + oldVersion);
        Log.d("Hello", "New version: " + newVersion);
    }
}
