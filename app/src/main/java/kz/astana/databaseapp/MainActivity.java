package kz.astana.databaseapp;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;

import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

import java.util.ArrayList;

import androidx.appcompat.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity {

    private final String DB_NAME = "animals";
    private final int version = 3;

    private SQLiteDatabase database;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        MyDatabaseHelper databaseHelper = new MyDatabaseHelper(MainActivity.this, DB_NAME, null, version);

        TextInputLayout idLayout = findViewById(R.id.idLayout);
        TextInputLayout valueLayout = findViewById(R.id.valueLayout);
        TextInputEditText id = findViewById(R.id.idEditText);
        TextInputEditText value = findViewById(R.id.valueEditText);
        Button save = findViewById(R.id.saveButton);
        Button update = findViewById(R.id.updateButton);
        Button delete = findViewById(R.id.deleteButton);
        Button load = findViewById(R.id.loadButton);
        Button search = findViewById(R.id.searchButton);
        Spinner spinner = findViewById(R.id.spinner);

        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (value.getText().length() == 0) {
                    valueLayout.setError("Enter value");
                } else {
                    database = databaseHelper.getWritableDatabase();
                    valueLayout.setError(null);
                    ContentValues contentValues = new ContentValues();
                    contentValues.put("name", value.getText().toString());
                    long rowId = database.insert("animals", null, contentValues);
                    Log.d("Hello", "Row ID: " + rowId);
                    database.close();
                }
            }
        });

        update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (id.getText().length() == 0 || value.getText().length() == 0) {
                    if (id.getText().length() == 0) {
                        idLayout.setError("Enter id");
                    }
                    if (value.getText().length() == 0) {
                        valueLayout.setError("Enter value");
                    }
                } else {
                    idLayout.setError(null);
                    valueLayout.setError(null);
                    database = databaseHelper.getWritableDatabase();
                    ContentValues contentValues = new ContentValues();
                    contentValues.put("name", value.getText().toString());
                    int count = database.update("animals", contentValues, "id = ?", new String[]{id.getText().toString()});
                    Log.d("Hello", "Update count: " + count);
                    database.close();
                }
            }
        });

        delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (id.getText().length() == 0) {
                    idLayout.setError("Enter id");
                } else {
                    idLayout.setError(null);
                    valueLayout.setError(null);
                    database = databaseHelper.getWritableDatabase();
                    int count = database.delete("animals", "id = ?", new String[]{id.getText().toString()});
                    Log.d("Hello", "Delete count:" + count);
                    database.close();
                }
            }
        });

        load.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                database = databaseHelper.getReadableDatabase();
                Cursor cursor = database.query("animals", null, null, null, null, null, null);
                ArrayList<String> spinnerItems = new ArrayList<>();
                if (cursor.moveToFirst()) {
                    int idIndex = cursor.getColumnIndex("id");
                    int nameIndex = cursor.getColumnIndex("name");
                    do {
                        int id = cursor.getInt(idIndex);
                        String name = cursor.getString(nameIndex);

                        spinnerItems.add("Id: " + id + ", Name: " + name);
                        Log.d("Hello", "ID: " + id + ", Text: " + name);
                    } while (cursor.moveToNext());
                } else {
                    Log.d("Hello", "Cursor is empty");
                }
                ArrayAdapter<String> adapter = new ArrayAdapter<>(MainActivity.this, android.R.layout.simple_spinner_item, spinnerItems);
                spinner.setAdapter(adapter);
                cursor.close();
                database.close();
            }
        });

        search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                database = databaseHelper.getReadableDatabase();
                Cursor cursor = database.query(
                        "animals",
                        new String[]{"name"},
                        "name = ?",
                        new String[]{value.getText().toString()},
                        null,
                        null,
                        null
                );
                ArrayList<String> spinnerItems = new ArrayList<>();
                if (cursor.moveToFirst()) {
                    int nameIndex = cursor.getColumnIndex("name");
                    do {
                        String name = cursor.getString(nameIndex);
                        spinnerItems.add(name);
                    } while (cursor.moveToNext());
                }
                ArrayAdapter<String> adapter = new ArrayAdapter<>(MainActivity.this, android.R.layout.simple_spinner_item, spinnerItems);
                spinner.setAdapter(adapter);

                cursor.close();
                database.close();
            }
        });

//      database = databaseHelper.getWritableDatabase();
//      database.execSQL("CREATE TABLE " + tableName.getText().toString() + "(id INTEGER PRIMARY KEY AUTOINCREMENT, name TEXT)");
//      database.close();

        // Это очищение таблицы, не удаление
//      int deleteCount = database.delete(tableName.getText().toString(), null, null);
    }
}